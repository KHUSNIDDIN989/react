FROM node:alpine as builder
WORKDIR /app

COPY ./package*.json ./

RUN npm ci

COPY . .

RUN npm run build

FROM nginx:alpine

EXPOSE 5173

COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

COPY --from=builder /app/dist /usr/share/nginx/html